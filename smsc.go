package smsc

import "net/http"
import "errors"
import "strconv"
import "strings"
import "net/url"
import "io/ioutil"
import "mime/multipart"
import "io"
import "bytes"
import "os"

import "fmt"

func SendEmail(login, password, email, from_email, subject, message string) error {
	params := url.Values{}
	params.Add("login", login)
	params.Add("psw", password)
	params.Add("phones", email)
	params.Add("mes", message)
	params.Add("sender", from_email)
	params.Add("subj", subject)
	params.Add("charset", "utf-8")
	params.Add("mail", "1")

	hc := &http.Client{}
	req, err := http.NewRequest("POST", "http://smsc.ru/sys/send.php", bytes.NewBufferString(params.Encode()))
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(params.Encode())))

	// resp, err := http.Get(url_cur.String())
	resp, err := hc.Do(req)
	if err != nil {
		return err
	}
	// robots, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return error(fmt.Errorf("%v", resp.StatusCode))
	}

	return nil
}

func SendAudioSMS(login, password, phones, file, sender string, id int) (int, float64, error) {
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	defer w.Close()
	// Add your image file
	f, err := os.Open(file)
	if err != nil {
		return 0, 0, err
	}
	defer f.Close()
	fw, err := w.CreateFormFile("file 1", file)
	if err != nil {
		return 0, 0, err
	}
	if _, err = io.Copy(fw, f); err != nil {
		return 0, 0, err
	}

	if fw, err = w.CreateFormField("login"); err != nil {
		return 0, 0, err
	}
	if _, err = fw.Write([]byte(login)); err != nil {
		return 0, 0, err
	}
	if fw, err = w.CreateFormField("psw"); err != nil {
		return 0, 0, err
	}
	if _, err = fw.Write([]byte(password)); err != nil {
		return 0, 0, err
	}
	if fw, err = w.CreateFormField("phones"); err != nil {
		return 0, 0, err
	}
	if _, err = fw.Write([]byte(phones)); err != nil {
		return 0, 0, err
	}
	if fw, err = w.CreateFormField("mes"); err != nil {
		return 0, 0, err
	}
	if _, err = fw.Write([]byte("<file 1> ")); err != nil {
		return 0, 0, err
	}
	if fw, err = w.CreateFormField("call"); err != nil {
		return 0, 0, err
	}
	if _, err = fw.Write([]byte("1")); err != nil {
		return 0, 0, err
	}
	if fw, err = w.CreateFormField("cost"); err != nil {
		return 0, 0, err
	}
	if _, err = fw.Write([]byte("2")); err != nil {
		return 0, 0, err
	}
	if fw, err = w.CreateFormField("fmt"); err != nil {
		return 0, 0, err
	}
	if _, err = fw.Write([]byte("1")); err != nil {
		return 0, 0, err
	}
	if sender != "" {
		if fw, err = w.CreateFormField("sender"); err != nil {
			return 0, 0, err
		}
		if _, err = fw.Write([]byte(sender)); err != nil {
			return 0, 0, err
		}
	}

	res, err := http.Post("http://smsc.ru/sys/send.php", w.FormDataContentType(), &b)
	if err != nil {
		fmt.Println("yopt0")
		return 0, 0, err
	}

	// fmt.Println(w.FormDataContentType())
	robots, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println("yopt2")
		return 0, 0, err
	}
	res.Body.Close()

	if res.StatusCode != 200 {
		return 0, 0, errors.New(res.Status)
	}

	resp_paraps := strings.Split(string(robots), ",")
	if resp_paraps[1][0] == '-' {
		switch resp_paraps[1][1:] {
		case "1":
			return 0, 0, errors.New("Ошибка в параметрах")
		case "2":
			return 0, 0, errors.New("Неверный логин или пароль")
		case "3":
			return 0, 0, errors.New("Недостаточно средств на счете Клиента")
		case "4":
			return 0, 0, errors.New("IP-адрес временно заблокирован из-за частых ошибок в запросах")
		case "5":
			return 0, 0, errors.New("Неверный формат даты")
		case "6":
			return 0, 0, errors.New("Сообщение запрещено (по тексту или по имени отправителя).")
		case "7":
			return 0, 0, errors.New("Неверный формат номера телефона")
		case "8":
			return 0, 0, errors.New("Сообщение на указанный номер не может быть доставлено")
		case "9":
			return 0, 0, errors.New("Отправка более одного одинакового запроса на передачу SMS-сообщения либо более пяти одинаковых запросов на получение стоимости сообщения в течение минуты")
		}
	}

	cost, _ := strconv.ParseFloat(resp_paraps[2], 64)
	count, _ := strconv.Atoi(resp_paraps[1])

	return count, cost, nil
}

// Отправка текстового смс сообщения
// login - Логин клиента
// password - Пароль Клиента или MD5-хеш пароля в нижнем регистре
// phones - Номер или разделенный запятой или точкой с запятой список номеров мобильных телефонов в международном формате, на которые отправляется сообщение
// mess - тело сообщения.
// sender - Имя отправителя, отображаемое в телефоне получателя. Разрешены английские буквы, цифры, пробел и некоторые символы. Длина – 11 символов или 15 цифр. Все имена регистрируются в личном кабинете. Для отключения Sender ID по умолчанию необходимо в качестве имени передать пустую строку.
// id - Идентификатор сообщения. Назначается Клиентом. Служит для дальнейшей идентификации сообщения. Если не указывать, то будет назначен автоматически. Не обязательно уникален. В случае 2-х одинаковых идентификаторов по запросу статуса будет возвращен статус последнего сообщения.
func SendTextSMS(login, password, phones, mess, sender string, id int) (int, int, float64, error) {
	// url_cur, err := url.Parse("http://smsc.ru/sys/send.php")

	params := url.Values{}
	params.Add("login", login)
	params.Add("psw", password)
	params.Add("phones", phones)
	params.Add("mes", mess)
	params.Add("charset", "utf-8")
	params.Add("cost", "2")
	params.Add("fmt", "1")
	if sender != "" {
		params.Add("sender", sender)
	}
	if id != 0 {
		params.Add("id", strconv.Itoa(id))
	}

	// url_cur.RawQuery = params.Encode()

	// fmt.Printf("%q\n", url)

	hc := &http.Client{}
	req, err := http.NewRequest("POST", "http://smsc.ru/sys/send.php", bytes.NewBufferString(params.Encode()))
	if err != nil {
		return 0, 0, 0, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(params.Encode())))

	// resp, err := http.Get(url_cur.String())
	resp, err := hc.Do(req)
	if err != nil {
		return 0, 0, 0, err
	}
	robots, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return 0, 0, 0, errors.New(resp.Status)
	}

	if string(robots) == "ERROR = 1 (parameters error)" {
		return 0, 0, 0, errors.New("Ошибка в параметрах")
	}

	resp_paraps := strings.Split(string(robots), ",")
	if resp_paraps[1][0] == '-' {
		switch resp_paraps[1][1:] {
		case "1":
			return 0, 0, 0, errors.New("Ошибка в параметрах")
		case "2":
			return 0, 0, 0, errors.New("Неверный логин или пароль")
		case "3":
			return 0, 0, 0, errors.New("Недостаточно средств на счете Клиента")
		case "4":
			return 0, 0, 0, errors.New("IP-адрес временно заблокирован из-за частых ошибок в запросах")
		case "5":
			return 0, 0, 0, errors.New("Неверный формат даты")
		case "6":
			return 0, 0, 0, errors.New("Сообщение запрещено (по тексту или по имени отправителя).")
		case "7":
			return 0, 0, 0, errors.New("Неверный формат номера телефона")
		case "8":
			return 0, 0, 0, errors.New("Сообщение на указанный номер не может быть доставлено")
		case "9":
			return 0, 0, 0, errors.New("Отправка более одного одинакового запроса на передачу SMS-сообщения либо более пяти одинаковых запросов на получение стоимости сообщения в течение минуты")
		}
	}

	sms_id, err := strconv.Atoi(resp_paraps[0])
	if err != nil {
		return 0, 0, 0, err
	}
	cost, _ := strconv.ParseFloat(resp_paraps[2], 64)
	count, _ := strconv.Atoi(resp_paraps[1])

	return sms_id, count, cost, nil
}

// Отправка голосового сообщения
// login - Логин клиента
// password - Пароль Клиента или MD5-хеш пароля в нижнем регистре
// phones - Номер или разделенный запятой или точкой с запятой список номеров мобильных телефонов в международном формате, на которые отправляется сообщение
// mess - тело сообщения.
// sender - Имя отправителя, отображаемое в телефоне получателя. Разрешены английские буквы, цифры, пробел и некоторые символы. Длина – 11 символов или 15 цифр. Все имена регистрируются в личном кабинете. Для отключения Sender ID по умолчанию необходимо в качестве имени передать пустую строку.
// m_type - Тип сообщения. 1 - Сообщение голосом, генерируемое из текста. 2 - сообщение в viber.
// id - Идентификатор сообщения. Назначается Клиентом. Служит для дальнейшей идентификации сообщения. Если не указывать, то будет назначен автоматически. Не обязательно уникален. В случае 2-х одинаковых идентификаторов по запросу статуса будет возвращен статус последнего сообщения.
func SendSMS(login, password, phones, mess, sender string, m_type, id int) (int, int, float64, error) {
	// url_cur, err := url.Parse("http://smsc.ru/sys/send.php")

	params := url.Values{}
	params.Add("login", login)
	params.Add("psw", password)
	params.Add("phones", phones)
	params.Add("mes", mess)
	params.Add("charset", "utf-8")
	params.Add("cost", "2")
	if m_type == 1 {
		params.Add("call", "1")
		params.Add("voice", "w2")
	}
	if m_type == 2 {
		params.Add("viber", "1")
	}
	params.Add("fmt", "1")
	if sender != "" {
		params.Add("sender", sender)
	}
	if id != 0 {
		params.Add("id", strconv.Itoa(id))
	}

	// url_cur.RawQuery = params.Encode()

	// fmt.Printf("%q\n", url)

	hc := &http.Client{}
	req, err := http.NewRequest("POST", "http://smsc.ru/sys/send.php", bytes.NewBufferString(params.Encode()))
	if err != nil {
		return 0, 0, 0, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(params.Encode())))

	// resp, err := http.Get(url_cur.String())
	resp, err := hc.Do(req)
	if err != nil {
		return 0, 0, 0, err
	}
	robots, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return 0, 0, 0, errors.New(resp.Status)
	}

	if string(robots) == "ERROR = 1 (parameters error)" {
		return 0, 0, 0, errors.New("Ошибка в параметрах")
	}

	resp_paraps := strings.Split(string(robots), ",")
	if resp_paraps[1][0] == '-' {
		switch resp_paraps[1][1:] {
		case "1":
			return 0, 0, 0, errors.New("Ошибка в параметрах")
		case "2":
			return 0, 0, 0, errors.New("Неверный логин или пароль")
		case "3":
			return 0, 0, 0, errors.New("Недостаточно средств на счете Клиента")
		case "4":
			return 0, 0, 0, errors.New("IP-адрес временно заблокирован из-за частых ошибок в запросах")
		case "5":
			return 0, 0, 0, errors.New("Неверный формат даты")
		case "6":
			return 0, 0, 0, errors.New("Сообщение запрещено (по тексту или по имени отправителя).")
		case "7":
			return 0, 0, 0, errors.New("Неверный формат номера телефона")
		case "8":
			return 0, 0, 0, errors.New("Сообщение на указанный номер не может быть доставлено")
		case "9":
			return 0, 0, 0, errors.New("Отправка более одного одинакового запроса на передачу SMS-сообщения либо более пяти одинаковых запросов на получение стоимости сообщения в течение минуты")
		}
	}

	sms_id, err := strconv.Atoi(resp_paraps[0])
	if err != nil {
		return 0, 0, 0, err
	}

	cost, _ := strconv.ParseFloat(resp_paraps[2], 64)
	count, _ := strconv.Atoi(resp_paraps[1])

	return sms_id, count, cost, nil
}

func GetSMSCost(login, password, phones, mess, sender string, m_type int) (float64, int, error) {
	// url_cur, err := url.Parse("http://smsc.ru/sys/send.php")

	params := url.Values{}
	params.Add("login", login)
	params.Add("psw", password)
	params.Add("phones", phones)
	params.Add("mes", mess)
	params.Add("charset", "utf-8")
	params.Add("cost", "1")
	if m_type == 1 {
		params.Add("call", "1")
		params.Add("voice", "w2")
	}
	if m_type == 2 {
		params.Add("viber", "1")
	}
	params.Add("fmt", "1")
	if sender != "" {
		params.Add("sender", sender)
	}

	// url_cur.RawQuery = params.Encode()

	// fmt.Printf("%q\n", url)

	hc := &http.Client{}
	req, err := http.NewRequest("POST", "http://smsc.ru/sys/send.php", bytes.NewBufferString(params.Encode()))
	if err != nil {
		return 0, 0, err
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(params.Encode())))

	// resp, err := http.Get(url_cur.String())
	resp, err := hc.Do(req)
	if err != nil {
		return 0, 0, err
	}
	robots, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return 0, 0, errors.New(resp.Status)
	}

	if string(robots) == "ERROR = 1 (parameters error)" {
		return 0, 0, errors.New("Ошибка в параметрах")
	}

	resp_paraps := strings.Split(string(robots), ",")
	if resp_paraps[1][0] == '-' {
		switch resp_paraps[1][1:] {
		case "1":
			return 0, 0, errors.New("Ошибка в параметрах")
		case "2":
			return 0, 0, errors.New("Неверный логин или пароль")
		case "3":
			return 0, 0, errors.New("Недостаточно средств на счете Клиента")
		case "4":
			return 0, 0, errors.New("IP-адрес временно заблокирован из-за частых ошибок в запросах")
		case "5":
			return 0, 0, errors.New("Неверный формат даты")
		case "6":
			return 0, 0, errors.New("Сообщение запрещено (по тексту или по имени отправителя).")
		case "7":
			return 0, 0, errors.New("Неверный формат номера телефона")
		case "8":
			return 0, 0, errors.New("Сообщение на указанный номер не может быть доставлено")
		case "9":
			return 0, 0, errors.New("Отправка более одного одинакового запроса на передачу SMS-сообщения либо более пяти одинаковых запросов на получение стоимости сообщения в течение минуты")
		}
	}

	cost, _ := strconv.ParseFloat(resp_paraps[0], 64)
	count, _ := strconv.Atoi(resp_paraps[1])

	return cost, count, nil
}

// Добавление имени отправителя
// login - Логин клиента
// password - Пароль Клиента или MD5-хеш пароля в нижнем регистре
// name - имя отправителя
func AddSenderName(login, password, name string) error {
	if len(name) > 11 {
		return errors.New("too long name")
	}

	url_cur, err := url.Parse("http://smsc.ru/sys/senders.php")
	if err != nil {
		return err
	}

	params := url.Values{}
	params.Add("add", "1")
	params.Add("login", login)
	params.Add("psw", password)
	params.Add("sender", name)
	params.Add("cmt", "test")
	params.Add("fmt", "1")

	url_cur.RawQuery = params.Encode()

	resp, err := http.Get(url_cur.String())
	if err != nil {
		return err
	}

	robots, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New(resp.Status)
	}

	resp_paraps := strings.Split(string(robots), ",")
	if resp_paraps[0] == "0" {
		switch resp_paraps[1][1:] {
		case "1":
			return errors.New("Ошибка в параметрах.")
		case "2":
			return errors.New("Неверный логин или пароль")
		case "3":
			return errors.New("Имя отправителя не найдено")
		case "4":
			return errors.New("IP-адрес временно заблокирован")
		case "5":
			return errors.New("Ошибка сохранения имени отправителя. Скорее всего данное имя уже используется!")
		case "7":
			return errors.New("Неверный формат номера")
		case "8":
			return errors.New("Код подтверждения на указанный номер не может быть доставлен")
		case "9":
			return errors.New("Попытка отправки более трех одинаковых запросов на получение списка доступных имен отправителей или пяти запросов на создание нового имени отправителя в течение минуты")
		case "10":
			return errors.New("Код уже был отправлен на указанный номер. Повторная попытка возможна через 8 часов")
		case "11":
			return errors.New("Неверный код подтверждения")
		}
	}

	return nil
}

// Удаление имени отправителя
// login - Логин клиента
// password - Пароль Клиента или MD5-хеш пароля в нижнем регистре
// name - имя отправителя
func DeleteSenderName(login, password, name string) error {
	url_cur, err := url.Parse("http://smsc.ru/sys/senders.php")
	if err != nil {
		return err
	}

	params := url.Values{}
	params.Add("del", "1")
	params.Add("login", login)
	params.Add("psw", password)
	params.Add("sender", name)
	params.Add("fmt", "1")

	url_cur.RawQuery = params.Encode()

	resp, err := http.Get(url_cur.String())
	if err != nil {
		return err
	}

	robots, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New(resp.Status)
	}

	if string(robots) != "OK" {
		resp_paraps := strings.Split(string(robots), ",")
		switch resp_paraps[1][1:] {
		case "1":
			return errors.New("Ошибка в параметрах.")
		case "2":
			return errors.New("Неверный логин или пароль")
		case "3":
			return errors.New("Имя отправителя не найдено")
		case "4":
			return errors.New("IP-адрес временно заблокирован")
		case "5":
			return errors.New("Ошибка сохранения имени отправителя. Скорее всего данное имя уже используется!")
		case "7":
			return errors.New("Неверный формат номера")
		case "8":
			return errors.New("Код подтверждения на указанный номер не может быть доставлен")
		case "9":
			return errors.New("Попытка отправки более трех одинаковых запросов на получение списка доступных имен отправителей или пяти запросов на создание нового имени отправителя в течение минуты")
		case "10":
			return errors.New("Код уже был отправлен на указанный номер. Повторная попытка возможна через 8 часов")
		case "11":
			return errors.New("Неверный код подтверждения")
		}
	}

	return nil
}

// Проверка наличия имени в списке одобренных имён
// login - Логин клиента
// password - Пароль Клиента или MD5-хеш пароля в нижнем регистре
// name - имя отправителя
func IsSenderNameApprove(login, password, name string) (bool, error) {
	// http://smsc.ru/sys/senders.php?get=1&login=<login>&psw=<password>
	url_cur, err := url.Parse("http://smsc.ru/sys/senders.php")
	if err != nil {
		return false, err
	}

	params := url.Values{}
	params.Add("get", "1")
	params.Add("login", login)
	params.Add("psw", password)

	url_cur.RawQuery = params.Encode()

	resp, err := http.Get(url_cur.String())
	if err != nil {
		return false, err
	}

	robots, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()

	resp_r := strings.Split(string(robots), "\n")

	for _, val := range resp_r {
		if val != "" {
			nam := strings.Split(val, "=")
			if name == nam[1][1:] {
				return true, nil
			}
		}
	}

	return false, nil
}
